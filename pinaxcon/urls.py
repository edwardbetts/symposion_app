import debug_toolbar
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView
from django.views.generic import TemplateView
from django.urls import include, path
from django.contrib.flatpages.views import flatpage

from django.contrib import admin

import symposion.views


urlpatterns = [
    path('saml2/', include('djangosaml2.urls')),
    path('admin/', admin.site.urls),

    path("speaker/", include("symposion.speakers.urls")),
    path("proposals/", include("symposion.proposals.urls")),
    path("reviews/", include("symposion.reviews.urls")),
    path("schedule/", include("symposion.schedule.urls")),
    path("conference/", include("symposion.conference.urls")),

    path("teams/", include("symposion.teams.urls")),
    path('raffle/', include("pinaxcon.raffle.urls")),

    # Required by registrasion
    path('tickets/payments/', include('registripe.urls')),
    path('tickets/', include('registrasion.urls')),
    path('nested_admin/', include('nested_admin.urls')),
    path('checkin/', include('regidesk.urls')),
    path('pages/', include('django.contrib.flatpages.urls')),

    path('dashboard/', symposion.views.dashboard, name="dashboard"),
    path('boardingpass', RedirectView.as_view(pattern_name="regidesk:boardingpass")),

    # Debug Toolbar. Always include to ensure tests work.
    path('__debug__/', include(debug_toolbar.urls)),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
