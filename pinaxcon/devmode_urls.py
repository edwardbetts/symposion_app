from django.contrib.auth.views import LoginView, LogoutView
from django.urls import include, path

from pinaxcon import urls

urlpatterns = [
    path('accounts/logout', LogoutView.as_view(template_name='admin/logout.html')),
    path('accounts/login', LoginView.as_view(template_name='admin/login.html')),
]

urlpatterns += urls.urlpatterns
