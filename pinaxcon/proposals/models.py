from django.db import models
from django.utils.translation import ugettext_lazy as _

from symposion.proposals.models import ProposalBase
from symposion.text_parser import parse


class Proposal(ProposalBase):

    TARGET_USER = 1
    TARGET_BUSINESS = 2
    TARGET_COMMUNITY = 3
    TARGET_DEVELOPER = 4

    TARGET_AUDIENCES = [
        (TARGET_DEVELOPER, "Developer"),
        (TARGET_COMMUNITY, "Community"),
        (TARGET_USER, "End User"),
        (TARGET_BUSINESS, "Business"),
    ]

    TOPIC_LINUX = 1
    TOPIC_SOFTWARE = 2
    TOPIC_HARDWARE = 3
    TOPIC_FIRMWARE = 4
    TOPIC_SYSADMIN = 5
    TOPIC_SECURITY = 6
    TOPIC_DOCUMENTATION = 7
    TOPIC_COMMUNITY = 8
    TOPIC_SCIENCE = 9
    TOPIC_GLAM = 10
    TOPIC_MULTIMEDIA = 11
    TOPIC_AEROSPACE = 12
    TOPIC_AGRICULTURE = 13
    TOPIC_OTHER = 14

    PROPOSAL_TOPIC = [
        (TOPIC_LINUX, "Linux"),
        (TOPIC_SOFTWARE, "Software"),
        (TOPIC_HARDWARE, "Hardware"),
        (TOPIC_FIRMWARE, "Firmware"),
        (TOPIC_SYSADMIN, "System Administration / Operations"),
        (TOPIC_SECURITY, "Security"),
        (TOPIC_DOCUMENTATION, "Documentation"),
        (TOPIC_COMMUNITY, "Community"),
        (TOPIC_SCIENCE, "Science & Data"),
        (TOPIC_GLAM, "Galleries, Libraries, Archives & Museums (GLAM)"),
        (TOPIC_MULTIMEDIA, "Multimedia"),
        (TOPIC_AEROSPACE, "Aerospace / UAV"),
        (TOPIC_AGRICULTURE, "Agriculture"),
        (TOPIC_OTHER, "Other"),
    ]

    LEVEL_BEGINNER = 1
    LEVEL_INTERMEDIATE = 2
    LEVEL_ADVANCED = 3

    EXPERIENCE_LEVEL = [
        (LEVEL_BEGINNER, "Beginner"),
        (LEVEL_INTERMEDIATE, "Intermediate"),
        (LEVEL_ADVANCED, "Advanced"),
    ]

    target_audience = models.IntegerField(
        choices=TARGET_AUDIENCES,
        help_text="Who is the target audience for your session?",
    )

    recording_release = models.BooleanField(
        default=True,
        help_text="I allow Linux Australia to release any recordings of "
        "presentations covered by this proposal, on YouTube under the "
        "standard YouTube licence, and on other platforms under the "
        "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International "
        "(<a href='https://creativecommons.org/licenses/by-nc-sa/4.0/'> "
        "CC BY-NC-SA 4.0</a>) licence."
    )

    materials_release = models.BooleanField(
        default=True,
        help_text="I allow Linux Australia to release any other material "
        "(such as slides) from presentations covered by this proposal, under "
        "the <a "
        "href='https://creativecommons.org/licenses/by-sa/3.0/au/deed.en'> "
        "Creative Commons Attribution-Share Alike Australia 3.0 Licence</a>"
    )

    primary_topic = models.IntegerField(
        choices=PROPOSAL_TOPIC,
        help_text="What is the primary topic area for your session?"
    )

    experience_level = models.IntegerField(
        choices=EXPERIENCE_LEVEL,
        help_text="What level of experience will your session be pitched at?"
    )

    require_approval = models.BooleanField(
        default=False,
        help_text="Do you require further approval from your employer or "
        "institution before you can confirm your availability to present?"
    )

    content_warning = models.TextField(
        "Content Warning",
        help_text=_("This will be shown on the schedule to give attendees "
                    "advanced warning of topics covered in the session. "),
        blank=True,
    )
    content_warning_html = models.TextField(blank=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.content_warning_html = parse(self.content_warning)
        return super(Proposal, self).save(*args, **kwargs)


class TalkProposal(Proposal):

    class Meta:
        verbose_name = "talk proposal"


class TutorialProposal(Proposal):

    class Meta:
        verbose_name = "tutorial proposal"
