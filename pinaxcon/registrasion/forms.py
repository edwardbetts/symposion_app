from pinaxcon.registrasion import models

from django import forms


class YesNoField(forms.TypedChoiceField):

    def __init__(self, *args, **kwargs):
        kwargs['required'] = True
        super(YesNoField, self).__init__(
            *args,
            coerce=lambda x: x in ['True', 'Yes', True],
            choices=((None, '--------'), (False, 'No'), (True, 'Yes')),
            **kwargs
        )


class ProfileForm(forms.ModelForm):
    ''' A form for requesting badge and profile information. '''

    required_css_class = 'label-required'

    class Meta:
        model = models.AttendeeProfile
        exclude = [
            'attendee',
            'children',
            'lca_announce',
            'lca_chat',
            'future_conference',
        ]
        widgets = {
            'past_lca': forms.widgets.CheckboxSelectMultiple
        }
        field_classes = {
            "of_legal_age": YesNoField,
        }
