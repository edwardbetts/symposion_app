from django.shortcuts import render, redirect

from account.decorators import login_required
from symposion.proposals.models import ProposalSection


@login_required
def dashboard(request):
    """
    Copy of symposion.views.dashboard modified to inject available proposal
    kinds into the request context.
    """

    if request.session.get("pending-token"):
        return redirect("speaker_create_token", request.session["pending-token"])


    sections = ProposalSection.available().prefetch_related('section__proposal_kinds')
    kinds = [k for kind in section for k in section.proposal_kinds]
    return render(request, "dashboard.html", {'kinds': kinds})

