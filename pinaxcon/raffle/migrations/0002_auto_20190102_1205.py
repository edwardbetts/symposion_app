# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2019-01-02 01:05
from __future__ import unicode_literals

from django.db import migrations


def get_admin_group_name(apps):
    from pinaxcon.raffle.apps import RaffleConfig
    return RaffleConfig.admin_group_name


def create_auth_group(apps, schema_editor):
    Group = apps.get_model("auth", "Group")
    Group.objects.get_or_create(name=get_admin_group_name(apps))


def delete_auth_group(apps, schema_editor):
    Group = apps.get_model("auth", "Group")
    Group.objects.filter(name=get_admin_group_name(apps)).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('pinaxcon_raffle', '0001_initial'),
        ('auth', '0001_initial')
    ]

    operations = [
        migrations.RunPython(create_auth_group, delete_auth_group),
    ]
