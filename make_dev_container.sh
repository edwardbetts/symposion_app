#!/bin/bash -x

IMAGE_NAME=${1:-symposion_app}

if [ -e ./symposion-tools ]; then
    pushd ./symposion-tools
    ./save_db_from_docker.sh
    popd
fi

# Check for docker running
if ! docker info >/dev/null 2>&1; then
    echo "Docker does not seem to be running. Please start it and retry."
    exit 1
fi

docker image build -f docker/Dockerfile -t ${IMAGE_NAME} --target symposion_dev .
docker container stop symposion
docker container rm symposion
docker container create --env-file docker/laptop-mode-env -p 28000:8000 -v $(pwd):/app/symposion_app --name symposion ${IMAGE_NAME}
docker container start symposion
## When we started the container and mounted . into /app/symposion_app, it hides the static/build directory
## As a kludge, re-run collectstatic to recreate it
## Possible alternative here: don't mount all of ., just mount the bits that we'd live to have update live
docker exec symposion ./manage.py collectstatic --noinput -v 0
docker exec symposion ./manage.py migrate
docker exec symposion ./manage.py loaddata ./fixtures/{conference,sites,sitetree,flatpages}.json
docker exec symposion ./manage.py create_review_permissions
docker exec symposion ./manage.py loaddata ./fixtures/sessions/*.json
docker exec symposion ./manage.py populate_inventory

if [ -e ./symposion-tools ]; then
    pushd ./symposion-tools
    ./fixture_to_docker.sh fixtures/dev_dummy_superuser.json
    ./fixture_to_docker.sh fixtures/????_*.json
    popd
else
    echo Now creating a Django superuser. Please enter a
    docker exec -it symposion ./manage.py createsuperuser --username admin1 --email root@example.com
fi

set +x
echo "Now you can log into http://localhost:28000/admin"
echo "Username: admin1      Password: the one you just typed twice"
echo "If you need to test as a non-admin user, create one at"
echo "http://localhost:28000/admin/auth/user/add/ - then log out"
echo "and log back in at http://localhost:28000"
