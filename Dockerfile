FROM symposion_app

COPY static/src/fonts/*.ttf /usr/local/share/fonts/
RUN fc-cache && fc-list

FROM symposion_base as symposion_dev
VOLUME /app/symposion_app
CMD ["./manage.py", "runserver", "-v3", "0.0.0.0:8000"]

FROM symposion_base as symposion_prod
CMD ["/usr/local/bin/uwsgi", "--http-socket", "0.0.0.0:8000", "-b", "8192", "--wsgi-file", "pinaxcon/wsgi.py"]