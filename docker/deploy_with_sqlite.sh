#!/bin/bash

/usr/local/bin/python /app/symposion_app/manage.py migrate
/usr/local/bin/python /app/symposion_app/manage.py loaddata /app/symposion_app/fixtures/{conference,sites,sitetree,flatpages}.json
/usr/local/bin/python /app/symposion_app/manage.py create_review_permissions
/usr/local/bin/python /app/symposion_app/manage.py loaddata /app/symposion_app/fixtures/????/*.json
#/usr/local/bin/python /app/symposion_app/manage.py populate_inventory

if [ -e /configs/auth/auth.json ]; then
    /usr/local/bin/python /app/symposion_app/manage.py loaddata /configs/auth/auth.json
elif [ $DATABASE_URL == "sqlite:////tmp/symposion.sqlite" ]; then
    /usr/local/bin/python /app/symposion_app/manage.py loaddata /app/symposion_app/fixtures/admin_users.json
fi

exec /usr/local/bin/uwsgi --http-socket 0.0.0.0:8000 --wsgi-file pinaxcon/wsgi.py
