FROM python:3.8-buster as symposion_base

RUN set -ex \
    && apt-get update
RUN set -ex \
    && buildDeps=' \
        libffi-dev \
        libfreetype6-dev \
        libjpeg-dev \
        libwebp-dev \
        libpng-dev \
        liblcms2-dev \
        zlib1g-dev \
        libmemcached-dev \
        libsasl2-dev \
        inkscape \
    ' \
    && apt-get install -y git xmlsec1 \
    && apt-get install -y $buildDeps --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

RUN set -ex \
    && pip install uwsgi

COPY constraints.txt requirements.txt /reqs/

RUN set -ex \
    && pip install -U pip \
    && pip install --no-cache-dir -r /reqs/requirements.txt -c /reqs/constraints.txt \
    && apt-get purge -y --auto-remove $buildDeps \
    && rm -rf /usr/src/python ~/.cache

COPY . /app/symposion_app

WORKDIR /app/symposion_app
RUN set -x \
    && pip install -r vendored_requirements.txt -c /reqs/constraints.txt
RUN set -x \
    && DJANGO_SECRET_KEY=1234 STRIPE_PUBLIC_KEY=1234 STRIPE_SECRET_KEY=1234 \
       DATABASE_URL="sqlite:////dev/null" python manage.py compilescss
RUN set -x \
    && DJANGO_SECRET_KEY=1234 STRIPE_PUBLIC_KEY=1234 STRIPE_SECRET_KEY=1234 \
       DATABASE_URL="sqlite:////dev/null" \
       python manage.py collectstatic --noinput -l -v 0
RUN set -ex \
    && cp static/build/fonts/*.ttf /usr/local/share/fonts/ \
    && fc-cache \
    && fc-list

FROM symposion_base as symposion_dev
VOLUME /app/symposion_app
CMD ["./manage.py", "runserver", "-v3", "0.0.0.0:8000"]

FROM symposion_base as symposion_prod
CMD ["/usr/local/bin/uwsgi", "--http-socket", "0.0.0.0:8000", "-b", "8192", "--wsgi-file", "pinaxcon/wsgi.py"]
