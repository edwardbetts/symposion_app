from django.db.models import Manager


class DefaultSelectRelatedManager(Manager):
    def get_queryset(self):
        symposion_meta = getattr(self.model, 'SymposionMeta', None)
        if symposion_meta is None:
            return super().get_queryset()

        select_related = getattr(symposion_meta, 'select_related', None)
        if select_related is None:
            return super().get_queryset()


        return super().get_queryset().select_related(*select_related)

