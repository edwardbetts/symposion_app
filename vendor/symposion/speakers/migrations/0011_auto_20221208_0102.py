# Generated by Django 2.2.28 on 2022-12-07 14:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('symposion_speakers', '0010_speaker_local_timezone'),
    ]

    operations = [
        migrations.AddField(
            model_name='speaker',
            name='mastodon_username',
            field=models.CharField(blank=True, help_text='Your Mastodon account', max_length=100),
        ),
        migrations.AddField(
            model_name='speaker',
            name='pronouns',
            field=models.CharField(blank=True, max_length=20, verbose_name='Pronouns'),
        ),
        migrations.AlterField(
            model_name='speaker',
            name='assistance',
            field=models.TextField(blank=True, help_text='We have budget set aside to provide financial assistance to speakers and attendees who might otherwise find it difficult to attend. Please provide details on why you require travel and/or accommodation assistance in order to present your proposed sessions. For travel assistance, please also tell us where you will be coming from (country, state, etc) to assist with planning.', verbose_name='Travel/Accommodation assistance details'),
        ),
        migrations.AlterField(
            model_name='speaker',
            name='travel_assistance',
            field=models.BooleanField(blank=True, default=False, help_text='Check this box if you require assistance to travel to the conference in order to present your proposed sessions.', verbose_name='Travel assistance required'),
        ),
    ]
