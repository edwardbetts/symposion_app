from django.contrib import admin

from reversion.admin import VersionAdmin

from symposion.teams.models import Team, Membership


class TeamAdmin(admin.ModelAdmin):
    prepopulated_fields={"slug": ("name",)}
    filter_horizontal = ('permissions', 'manager_permissions',)


admin.site.register(Team, TeamAdmin)


class MembershipAdmin(VersionAdmin):
    list_display = ["team", "user", "state"]
    list_filter = ["team","state"]
    search_fields = ["user__username"]


admin.site.register(Membership, MembershipAdmin)
