import copy
from regidesk import models

import functools

from django import forms
from django.core.exceptions import ValidationError
from django.db.models import F, Q
from django.forms import widgets
from django.urls import reverse
from django.utils import timezone

from django_countries import countries
from django_countries.fields import LazyTypedChoiceField
from django_countries.widgets import CountrySelectWidget

