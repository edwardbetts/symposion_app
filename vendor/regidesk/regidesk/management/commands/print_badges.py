#!/usr/bin/env python

from django.core.management.base import BaseCommand, CommandError

from django.contrib.auth import get_user_model
from registrasion.views import _convert_img as convert_img
from registrasion.views import render_badge_svg

User = get_user_model()


class Command(BaseCommand):

    def handle(self, *args, **options):

        users = User.objects.filter(
            checkin__checked_in_bool=True).filter(
            checkin__badge_printed=False
        )

        for user in users:

            try:
                svg = render_badge_svg(user, overlay=True)
                rendered = convert_img(svg, outformat="pdf")
                with open("/app/badge_out/{}.pdf".format(user.checkin.checkin_code), "wb") as outfile:
                    outfile.write(rendered)
                user.checkin.mark_badge_printed()
            except Exception:
                pass
