from django.contrib import admin

# Register your models here.
from regidesk.models import BoardingPassTemplate, BoardingPass, CheckIn

admin.site.register(
    BoardingPassTemplate,
    list_display=['label','from_address','subject']
)

admin.site.register( BoardingPass,
                     list_display=['to_address','created','sent'],
                     search_fields=['to_address'],
                     filter_fields=['created','sent'],
                     readonly_fields=['created','sent',
                                      'template', 'to_address', 'from_address',
                                      'subject', 'body','html_body' ]
)

admin.site.register(
    CheckIn,
    list_display=['user','seen','checked_in','checkin_code','checked_in_bool','badge_printed','schwag_given','needs_review'],
    search_fields=['user__username','checkin_code'],
    list_filter=['seen','checked_in',"checked_in_bool","badge_printed","schwag_given"],
    readonly_fields=['user','seen','checked_in','checkin_code']
)
