Steps for creating a miniconf
=============================

In code:

* Add a ProposalForm - pinaxcon.proposals.forms
* Add a Model - pinaxcon.proposals.models
* Register models - pinxacon.proposals.admin
* Map slug to proposal form - pinaxcon.settings.py

In the UI:

* Add a section
* Add a ProposalSection
* Add a Kind

On the commandline:

* Run manage.py create_review_permissions

In the Admin UI:

* Add a team to manage reviews

